jQuery(document).on('click','.tabcontent a, a.icon-play-btn',function() {
  jQuery('.popup-main').addClass('popup-active');
  var videolink = jQuery(this).attr('data-video-link').replace('https://youtu.be/','');
  jQuery('.popup-content iframe#youtbevideo').attr('src','https://www.youtube.com/embed/'+videolink);
});
jQuery(document).on('click','.popup-main .popup-inner .colse-icon',function() {
  jQuery('.popup-main').removeClass('popup-active');
});


// slider-popup start
jQuery(document).on('click','.img-must-section a, a.celebrities-img',function() {
  jQuery('.slider-popup-main').addClass('slider-popup-active');
  jQuery('.slider-for, .slide-tab').slick('refresh');
});
jQuery(document).on('click','.slider-popup-main .slider-popup-inner .slider-colse-icon',function() {
  jQuery('.slider-popup-main').removeClass('slider-popup-active');
});
// slider-popup end


jQuery(document).ready(function(){
  jQuery('.your-class').slick();
});

jQuery('.your-class').slick({
	dots: true,
	arrows: false
});


jQuery(document).ready(function(){
 jQuery('.multiple-items-2').slick({
   centerMode: true,
  centerPadding: '150px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        dots: true,
        prevArrow: false,
        nextArrow: false
      }
    }
  ]
});
jQuery('.multiple-items').slick({
   centerMode: true,
  centerPadding: '0px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 1,
        dots: true,
        prevArrow: false,
        nextArrow: false
      }
    }
  ]
});

jQuery('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slide-tab'
});
jQuery('.slide-tab').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  arrows: true,
  centerMode: true,
  focusOnSelect: true
});

var is_opera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var is_Edge = navigator.userAgent.indexOf("Edge") > -1;
    var is_chrome = !!window.chrome && !is_opera && !is_Edge;
    var is_explorer= typeof document !== 'undefined' && !!document.documentMode && !is_Edge;
    var is_firefox = typeof window.InstallTrigger !== 'undefined';
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(is_chrome){
      jQuery('body').addClass('chrome');
    }else if( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)){
     jQuery('body').addClass('safari');
    }else if(is_firefox){
       jQuery('body').addClass('firefox');
    }

});



function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();


